###
# Le Müsik mongoose Schemas Written in Coffeescript
# v 1.0.0
# Mikko Majuri (majuri.mikko@gmail.com)
###

mongoose = require 'mongoose'
Schema = mongoose.Schema

SongSchema = new Schema
	name				:		{type: String}
	url 				:		{type: String}

Song = mongoose.model 'Song', SongSchema

exports.Song	=	Song
