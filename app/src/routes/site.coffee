###
# Le Müsik routes for site. Written in Coffeescript
# v 1.0.0
# Mikko Majuri (majuri.mikko@gmail.com)
###

#basic index routing

exports.index = (req, res) ->
	res.render 'index'
