###
# Le Müsik Backbone.js song view Written in Coffeescript
# v 1.0.0
# Mikko Majuri (majuri.mikko@gmail.com)
###
jade = require 'jade'

window.SongView = Backbone.View.extend ->

	template = jade.compile $('#songDetails').text()

	render			: (eventName) ->
		$(@el).html @template @model.toJSON()
		return @

window.SongListItemView = Backbone.View.extend ->

	tagName			: 'li'

	template		= jade.compile $('#wineListItem').text()

	render			: (eventName) ->
		$(@el).html @template @model.toJSON()
		return @

window.SongListView = Backbone.View.extend ->

	tagName			:	'ul'

	initialize	:	->
		@model.bind 'reset', @render, @

	render			:	(eventName) ->
		_.each @model.models, (song) ->
			$(@el).append new SongListItemView({model:wine}).render el
		, @
		return @
