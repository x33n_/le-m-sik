###
# Le Müsik Backbone.js app entrypoint Written in Coffeescript
# v 1.0.0
# Mikko Majuri (majuri.mikko@gmail.com)
###
sr = require '/routers/songRouter'

app = new sr.SongRouter()
Backbone.history.start
