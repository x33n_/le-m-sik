###
# Le Müsik Backbone.js song collection. Written in Coffeescript
# v 1.0.0
# Mikko Majuri (majuri.mikko@gmail.com)
###

song = require '../models/songModel'

window.songCollection = Backbone.Collection.extend ->
	model	: song.Song
	url		: '../api/songs'
