###
# Le Müsik Backbone.js song model. Written in Coffeescript
# v 1.0.0
# Mikko Majuri (majuri.mikko@gmail.com)
###

window.Song = Backbone.Model.extend()
