LE MÜSIK
========

The inspiration
---------------
This project is built for a loose group of friends who share a common love for music.

The group has been using facebook as a means to share music found on different website throughout the internet.

Facebook however offers a horrible platform for sharing music. There are no search functionalities. Now way of categorizing the music and it lacks continous play. What it has is a good way for communicating within the group and it is actively used by everyone in the group, so the shared music has a good reach.

The aim of this project
-----------------------
The end product of this project is going to be a website where:

-	Anyone from the group can post a link to a song
-	The songs can be categorized and given tags
-	The songs can be searched
  -	By name
  -	By artist
  -	By category
  -	By tags
  -	By poster
-	The post will appear in the facebook group as well
-	Any songs posted in the facebook group will appear in the service
-	There is a possibility to create playlists
-	There is a possibility for continous play

The technologies
----------------
This project will be produced in coffeescript with a backend running on node.js and a frontend done with backbone.js. The necessary databases will be mongodb. 

The project will be done following TDD-principles and testing will be performed with mocha and chai.
